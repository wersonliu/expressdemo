var express = require('express');
// var bodyParser=require('body-parser');

var router = express.Router();
// var session = require('express-session');
var MongoClient = require('mongodb').MongoClient;

var url = 'mongodb://localhost:27017/test';

/* GET home page. */
router.post('/', function (req, res, next) {
    console.log(req.body);
    MongoClient.connect(url, function (err, db) {
        if (err) {
            console.log(err);
            return;
        }
        //{"username":req.body.username,"password":req.body.password}
        const mydb=db.db('test');
        var result = mydb.collection('productmanage').find(req.body);
        result.toArray(function (err,data) {
            if (data.length > 0) {
                console.log('success');
                req.session.userinfo=data[0];
                // res.redirect('/product');  /*登录成功跳转到商品列表*/
                // res.send('<script>location.href="/product"</script>')
                res.redirect('/product')
            } else {
                console.log('fail');
                res.send('<script>alert("登陆失败");location.href="/login"</script>')
            }
        })

    });
    res.render('login');
});

module.exports = router;
